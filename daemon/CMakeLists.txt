
set(qt_SRCS
    callchannelobserver.cpp
    emblemcountmanager.cpp
    historydaemon.cpp
    historyservicedbus.cpp
    pluginmanager.cpp
    rolesinterface.cpp
    textchannelobserver.cpp
    )

set(daemon_SRCS main.cpp ${qt_SRCS})

include_directories(
    ${TP_QT5_INCLUDE_DIRS}
    ${CMAKE_SOURCE_DIR}/src
    ${CMAKE_CURRENT_BINARY_DIR}
    ${CMAKE_CURRENT_SOURCE_DIR}
    )

qt5_add_dbus_adaptor(daemon_SRCS HistoryService.xml historyservicedbus.h HistoryServiceDBus)

add_executable(lomiri-history-daemon ${daemon_SRCS} ${daemon_HDRS})

target_link_libraries(lomiri-history-daemon
    Qt5::Core
    Qt5::DBus
    ${TP_QT5_LIBRARIES}
    lomirihistoryservice
    )

configure_file(org.freedesktop.Telepathy.Client.HistoryDaemonObserver.service.in org.freedesktop.Telepathy.Client.HistoryDaemonObserver.service)
configure_file(com.lomiri.HistoryService.service.in com.lomiri.HistoryService.service)
configure_file(lomiri-history-daemon.service.in ${CMAKE_CURRENT_BINARY_DIR}/lomiri-history-daemon.service)

install(TARGETS lomiri-history-daemon RUNTIME DESTINATION ${CMAKE_INSTALL_FULL_LIBEXECDIR})
install(FILES
        ${CMAKE_CURRENT_BINARY_DIR}/org.freedesktop.Telepathy.Client.HistoryDaemonObserver.service
        ${CMAKE_CURRENT_BINARY_DIR}/com.lomiri.HistoryService.service
        DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/dbus-1/services
        )
install(FILES LomiriHistoryDaemonObserver.client DESTINATION share/telepathy/clients)
if(USE_SYSTEMD)
    pkg_get_variable(SYSTEMD_USER_UNIT_DIR systemd systemduserunitdir)
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/lomiri-history-daemon.service
            DESTINATION ${SYSTEMD_USER_UNIT_DIR})
endif()
