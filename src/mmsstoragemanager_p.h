/*
 * Copyright (C) 2022 Ubports Foundation.
 *
 * Authors:
 *  Lionel Duboeuf <lduboeuf@ouvaton.org>
 *
 * This file is part of lomiri-history-service.
 *
 * lomiri-history-service is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * lomiri-history-service is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MMSSTORAGEMANAGER_H
#define MMSSTORAGEMANAGER_H

#pragma GCC diagnostic push
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
#include <TelepathyQt/TextChannel>
#pragma GCC diagnostic pop

namespace History
{

class MmsStorageManager
{
public:
    static MmsStorageManager *instance();

    QString mmsStoragePath(const QString &accountId, const QString &threadId = QString(), const QString &eventId = QString()) const;
    bool removeAttachments(const QString &accountId, const QString &threadId, const QString &eventId = QString());
    bool removeAttachments(QList<QString> files);
    bool removeAttachmentsFromThreads(const QList<QVariantMap> &threads);
    bool removeAttachmentsFromEvents(const QList<QVariantMap> &events);
    QString saveAttachment(const Tp::MessagePart &part, const QString &accountId, const QString &threadId, const QString &eventId) const;
    QString storageRootPath() const;

private:
    explicit MmsStorageManager();

    QString mStoragePath;
};

}

#endif // MMSSTORAGEMANAGER_H
